# Intro to Gitlab CI and CD with deployer

## References

### Tools
- [CodeFlow](http://getcodeflow.com)
- [PhpStan](https://packagist.org/packages/phpstan/phpstan)
- [Parallel lin](https://packagist.org/packages/jakub-onderka/php-parallel-lint)

### Gitlab CI
- [About Gitlab CI](https://about.gitlab.com/product/continuous-integration/)
- [Documentation](https://docs.gitlab.com/ee/ci/)
- [.gitlab-ci.yml reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
- [Leading CI platform](https://about.gitlab.com/2017/09/27/gitlab-leader-continuous-integration-forrester-wave/)
- [Gitlab CI for Github](https://docs.gitlab.com/ee/ci/ci_cd_for_external_repos/github_integration.html)
- [Install specific CI runner](https://docs.gitlab.com/runner/install/linux-repository.html#installing-the-runner)
- [Runner documentation](https://docs.gitlab.com/runner/)

### Deploy
- [DG FTP Deploy](https://github.com/dg/ftp-deployment)
- [Deployer](https://deployer.org)
- [DPL](https://github.com/travis-ci/dpl)
- [SCP](https://docs.gitlab.com/ee/ci/examples/deployment/composer-npm-deploy.html)
- [Docker deployment](http://paislee.io/how-to-automate-docker-deployments/)   

**[!! Examples !!](https://docs.gitlab.com/ee/ci/examples/README.html)**
    

## Tips/tricks

- [Slack integration/notifications](https://docs.gitlab.com/ee/user/project/integrations/slack.html)
- CRON to clean runner cache:   
    `0 3 * * * find /cache -maxdepth 1 -mtime +5 -exec rm -rf {} \;`