<?php
namespace Deployer;

require 'recipe/common.php';

set('application', 'test_app');

// Project repository
set('repository', '');
set('allow_anonymous_stats', false);

host('185.14.184.107')
	->user('root')
	->port(22)
	->configFile('~/.ssh/config')
	->identityFile('~/.ssh/id_rsa')
	->forwardAgent(true)
    ->set('deploy_path', '/var/test-deployer')
	->addSshOption('StrictHostKeyChecking', 'no');

task('deploy', [
    'deploy:info',
	'deploy:update_code'
]);
